#!/bin/sh -e

if [ ! -d gecko-b2g ]; then
	git clone --depth 1 https://github.com/kaiostech/gecko-b2g
fi

deps="
	alsa-lib-dev
	build-base
	dbus-glib-dev
	gtk+3.0-dev
	pulseaudio-dev
	rust
	"

hostdeps="
	binutils-aarch64
	cargo 
	cbindgen
	clang 
	clang-libclang
	git
	lld 
	llvm
	m4
	nodejs
	python3	
	sed
	tar
	zip
	"
	

if [ -n "$GITLAB_CI" ]; then
	apk add $deps $hostdeps
	mkdir -p /sysroot-aarch64/etc/apk
	cp -r /etc/apk/keys /etc/apk/repositories /sysroot-aarch64/etc/apk/
	cp /usr/share/apk/keys/aarch64/* /sysroot-aarch64/etc/apk/keys/
	apk add --root /sysroot-aarch64 --initdb --no-scripts --arch aarch64 $deps
	ln -sf "$PWD/gecko-b2g" /mnt/gecko
	ln -sf "$PWD/gecko-patches" /var/cache/gecko-patches
	ln -sf /sysroot-aarch64/usr/lib/rustlib/aarch64-alpine-linux-musl /usr/lib/rustlib/
	sh buildscript.sh "$@"
else
	export ENABLE_SCCACHE="ac_add_options --with-ccache=sccache"
	chroot=$(pmbootstrap config work)/chroot_native
	sysroot=$(pmbootstrap config work)/chroot_buildroot_aarch64

	pmbootstrap -q chroot -- apk -q add $deps $hostdeps sccache
	pmbootstrap -q chroot -baarch64 -- apk -q add $deps
	sudo mkdir -p "$chroot/mnt/gecko"
	if mountpoint -q "$chroot/mnt/gecko"; then
		sudo umount "$chroot/mnt/gecko"
	fi
	sudo mount --bind "$PWD/gecko-b2g" "$chroot/mnt/gecko"
	sudo mkdir -p "$chroot/sysroot-aarch64"
	if mountpoint -q "$chroot/sysroot-aarch64"; then
		sudo umount "$chroot/sysroot-aarch64"
	fi
	sudo mount --bind "$sysroot" "$chroot/sysroot-aarch64"
	pmbootstrap -q chroot -- ln -fs /sysroot-aarch64/usr/lib/rustlib/aarch64-alpine-linux-musl /usr/lib/rustlib/
	pmbootstrap -q chroot -- chown -R pmos:pmos /mnt/gecko
	sudo install -Dm644 gecko-patches/* -t "$chroot/var/cache/gecko-patches"
	sudo install -m755 buildscript.sh "$chroot/usr/bin/build-b2g"
	pmbootstrap -q chroot --user -- build-b2g "$@"
fi
