#!/bin/sh

cd /mnt/gecko

if [ "$1" = "--git-reset" ]; then
	exec git reset --hard
elif [ "$1" = "--git-status" ]; then
	exec git status
elif [ -n "$(git status --porcelain | grep -v '^??')" ]; then
	echo "$0: INFO: Git tree is dirty" >&2
	echo "$0: Pass --git-reset to this script to revert all patches" >&2
	echo "$0: Pass --git-status to see what was changed" >&2
else
	echo "$0: INFO: Applying patches..." >&2
	for patch in /var/cache/gecko-patches/*; do
		echo "$0: >>>>: $patch..."
		patch -p1 < "$patch"
	done
	sed -i 's/ uint(/ unsigned(/g' dom/b2g/alarm/AlarmManager.cpp
	cat >.cargo/config.toml <<EOF
paths = ["/mnt/gecko/audio_thread_priority_patch"]
EOF
	rm -rf audio_thread_priority_patch
	cp -r third_party/rust/audio_thread_priority audio_thread_priority_patch
	sed -i 's/rlimit64/rlimit/g' audio_thread_priority_patch/src/rt_linux.rs
fi

arch=aarch64
sysroot=/sysroot-$arch
target=$arch-alpine-linux-musl
gcc_prefix=$sysroot/usr/lib/gcc/$target
gcc_version=$(ls $gcc_prefix | tail -n 1)
gcc_dir="$gcc_prefix/$gcc_version"

cat >mozconfig-b2g-pmos <<EOF
ac_add_options --enable-application=b2g
ac_add_options --host=x86_64-alpine-linux-musl
ac_add_options --target=$target
ac_add_options --sysroot=$sysroot
ac_add_options --enable-linker=lld
ac_add_options --with-app-basename=b2g
ac_add_options --enable-default-toolkit=cairo-gtk3-wayland-only
ac_add_options --with-libclang-path=/usr/lib
CC="clang -B /usr/$target/bin/ --gcc-install-dir=$gcc_dir"
CXX="clang++ -B /usr/$target/bin/ --gcc-install-dir=$gcc_dir"

# fails to build with musl
ac_add_options --disable-jemalloc

ac_add_options --disable-crashreporter

# Disable a bunch of non critical stuff
ac_add_options --disable-printing
ac_add_options --disable-updater
# Disable telemetry
ac_add_options MOZ_TELEMETRY_REPORTING=

# Enable Rust <-> C++ LTO
ac_add_options --enable-lto
export MOZ_LTO=cross

# Optimize the build
ac_add_options --disable-profiling
ac_add_options --enable-small-chunk-size

# Use sccache
$ENABLE_SCCACHE

# Auto-clobber
mk_add_options AUTOCLOBBER=1

# Pretend to be an official build to be in a release configuration.
export MOZILLA_OFFICIAL=1

ac_add_options --enable-profiling

# Fails to spawn processes on Alpine for some reason
#ac_add_options --enable-forkserver

# These features currently require Gonk
ac_add_options --disable-wifi-support
#ac_add_options --enable-b2g-camera
#ac_add_options --enable-b2g-ims
#ac_add_options --enable-b2g-fm
#ac_add_options --enable-b2g-mediadrm
#ac_add_options --enable-b2g-ril
#ac_add_options --enable-b2g-voice-processing
#ac_add_options --enable-b2g-bt

ac_add_options --with-b2g-os-name="KaiOS"

ac_add_options --disable-sandbox

ac_add_options --enable-wasm-function-references
ac_add_options --enable-wasm-gc

# Disable WASM libraries sandboxing for now until we figure out build failures.
ac_add_options --without-wasm-sandboxed-libraries
# ac_add_options --wasi-sysroot=/usr/share/wasi-sysroot
EOF

export MOZCONFIG=mozconfig-b2g-pmos
export MOZ_OBJDIR=obj-pmos

./mach "$@"
