# Build B2G for postmarketOS

The `invoke-mach.sh` script uses pmbootstrap to set up an environment for building B2G and invokes the `mach` command in that environment.

Use `./invoke-mach.sh build` to build Gecko and `./invoke-mach.sh package` to produce a tarball.
The tarball will be in `gecko-b2g/obj-pmos/dist`.
